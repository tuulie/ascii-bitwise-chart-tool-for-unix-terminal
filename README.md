Copyright (C) 2023 Tuuli Kokkonen, GPL
The program is distributed under the terms of the GNU General Public License.


Run program from a UNIX-based terminal with 
./program_name parameters

![program capture](bit_chart_img.png)

![program capture](bit_chart.png)

Running program without parameters prints out manual.

This program prints out charts (tables) of bitwise operation results with the corresponding ASCII symbols and decimal and binary representations.

The binary representation of negative numbers uses 2's complement.

The program has two highlighting options for results.

The compare option shows wether the result is smaller (blue), equal (orange) or greater (red) than
the previous result.

The limit option shows wether the result is smaller (blue), equal (orange) or greater (red) than the value input by the user.
