#include <stdio.h>
#include <stdlib.h>
#include <sys/ioctl.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>

/* Symbol Bitwise Chart Copyright (C) Tuuli Kokkonen 2023, 2024 GPL 	*/
/* This program is distributed under the terms of the GNU		*/
/* General Public License.						*/

/* This program prints an ASCII bitwise operation result table. 	*/
/* Running program without arguments prints manual. 			*/

/* compile with: gcc -o bit_chart bit_chart.c */

struct Params_t {
	int input_val;
	
	int bin_start;

	int limit_bool;
	int limit_val;
	int compare_bool;
	
	int range_start;
	int range_end;
};

typedef struct Params_t Chart_params_t;

char ASCII_chars[] = {'N', 'U', 'L', '\0', 'S', 'O', 'H', '\0', 
	'S', 'T', 'X', '\0', 'E', 'T', 'X', '\0', 'E', 'O', 'T', '\0', 
	'E', 'N', 'Q', '\0', 'A', 'C', 'K', '\0', 'B', 'E', 'L', '\0', 
	'B', 'S', ' ', '\0', 'H', 'T', ' ', '\0', 'L', 'F', ' ', '\0', 
	'V', 'T', ' ', '\0', 'F', 'F', ' ', '\0', 'C', 'R', ' ', '\0', 
	'S', '0', ' ', '\0', 'S', 'I', ' ', '\0', 'D', 'L', 'E', '\0', 
	'D', 'C', '1', '\0', 'D', 'C', '2', '\0', 'D', 'C', '3', '\0', 
	'D', 'C', '4', '\0', 'N', 'A', 'K', '\0', 'S', 'Y', 'N', '\0', 
	'E', 'T', 'B', '\0', 'C', 'A', 'N', '\0', 'E', 'M', ' ', '\0', 
	'S', 'U', 'B', '\0', 'E', 'S', 'C', '\0', 'F', 'S', ' ', '\0', 
	'G', 'S', ' ', '\0', 'R', 'S', ' ', '\0', 'U', 'S', ' ', '\0', 
	'S', 'P', 'A', '\0', '!', ' ', ' ', '\0', '\"', ' ', ' ', '\0', 
	'#', ' ', ' ', '\0', '$', ' ', ' ', '\0', '%', ' ', ' ', '\0', 
	'&', ' ', ' ', '\0', '\'', ' ', ' ', '\0', '(', ' ', ' ', '\0', 
	')', ' ', ' ', '\0', '*', ' ', ' ', '\0', '+', ' ', ' ', '\0', 
	',', ' ', ' ', '\0', '-', ' ', ' ', '\0', '.', ' ', ' ', '\0', 
	'/', ' ', ' ', '\0', '0', ' ', ' ', '\0', '1', ' ', ' ', '\0', 
	'2', ' ', ' ', '\0', '3', ' ', ' ', '\0', '4', ' ', ' ', '\0', 
	'5', ' ', ' ', '\0', '6', ' ', ' ', '\0', '7', ' ', ' ', '\0', 
	'8', ' ', ' ', '\0', '9', ' ', ' ', '\0', ':', ' ', ' ', '\0', 
	';', ' ', ' ', '\0', '<', ' ', ' ', '\0', '=', ' ', ' ', '\0', 
	'>', ' ', ' ', '\0', '?', ' ', ' ', '\0', '@', ' ', ' ', '\0', 
	'A', ' ', ' ', '\0', 'B', ' ', ' ', '\0', 'C', ' ', ' ', '\0', 
	'D', ' ', ' ', '\0', 'E', ' ', ' ', '\0', 'F', ' ', ' ', '\0', 
	'G', ' ', ' ', '\0', 'H', ' ', ' ', '\0', 'I', ' ', ' ', '\0', 
	'J', ' ', ' ', '\0', 'K', ' ', ' ', '\0', 'L', ' ', ' ', '\0', 
	'M', ' ', ' ', '\0', 'N', ' ', ' ', '\0', 'O', ' ', ' ', '\0', 
	'P', ' ', ' ', '\0', 'Q', ' ', ' ', '\0', 'R', ' ', ' ', '\0', 
	'S', ' ', ' ', '\0', 'T', ' ', ' ', '\0', 'U', ' ', ' ', '\0', 
	'V', ' ', ' ', '\0', 'W', ' ', ' ', '\0', 'X', ' ', ' ', '\0', 
	'Y', ' ', ' ', '\0', 'Z', ' ', ' ', '\0', '[', ' ', ' ', '\0', 
	'\\', ' ', ' ', '\0', ']', ' ', ' ', '\0', '^', ' ', ' ', '\0', 
	'_', ' ', ' ', '\0', '`', ' ', ' ', '\0', 'a', ' ', ' ', '\0', 
	'b', ' ', ' ', '\0', 'c', ' ', ' ', '\0', 'd', ' ', ' ', '\0', 
	'e', ' ', ' ', '\0', 'f', ' ', ' ', '\0', 'g', ' ', ' ', '\0', 
	'h', ' ', ' ', '\0', 'i', ' ', ' ', '\0', 'j', ' ', ' ', '\0', 
	'k', ' ', ' ', '\0', 'l', ' ', ' ', '\0', 'm', ' ', ' ', '\0', 
	'n', ' ', ' ', '\0', 'o', ' ', ' ', '\0', 'p', ' ', ' ', '\0', 
	'q', ' ', ' ', '\0', 'r', ' ', ' ', '\0', 's', ' ', ' ', '\0', 
	't', ' ', ' ', '\0', 'u', ' ', ' ', '\0', 'v', ' ', ' ', '\0', 
	'w', ' ', ' ', '\0', 'x', ' ', ' ', '\0', 'y', ' ', ' ', '\0', 
	'z', ' ', ' ', '\0', '{', ' ', ' ', '\0', '|', ' ', ' ', '\0', 
	'}', ' ', ' ', '\0', '~', ' ', ' ', '\0', 'D', 'E', 'L', '\0'};

char empty_val[] = {' ', ' ', ' ', '\0'};

void fill_field(int range_n, char *i_c, int a, char *a_c, 
				int field_w, char *field) {
	int entry_len = (field_w - 4) / 4;
	char *str_ptr = i_c;
	int start = entry_len + 1;
	
	char n_str[12];
	char a_str[12];

	sprintf(n_str, "%d", range_n);
	sprintf(a_str, "%d", a);

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 3; j++) {
			field[start+j] = str_ptr[j];
		}
		str_ptr = a_c;
		start = (entry_len * 3)+2;
	}
	start = 1;
	int l = strlen(n_str);
	str_ptr = n_str;
	
	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < l; j++) {
			field[start+j] = str_ptr[j];
		}
		l = strlen(a_str);
		str_ptr = a_str;
		start = (entry_len * 2)+2;
	}
}

int check_sign(char check_char, int i) {
	if (i > 0) {
		return 0;
	}
	else if (check_char == 45) {
		return 1;
	}
	else {
		return 0;
	}
}

int check_number(char *check_val) {

	int n_len = strlen(check_val);
	
	for (int i = 0; i < n_len; i++) {
		if (!isdigit(check_val[i]) && 
		    !check_sign(check_val[i], i)) {
			printf("ERR: argument is not a number.\n");
			exit(1);
		}
	}

	int n = atoi(check_val);
	return n;
}

void print_binaries(int start, int bin_len, int *bin_arr) {
	for (int i = start; i < bin_len; i++) {
		printf("%d", bin_arr[i]);
	}
	printf(" ");
}

char* get_ASCII_value(int val) {
	char *i_c;
	if (val >= 0 && val < 128) {
		int idx = val * 4;
		i_c = &ASCII_chars[idx];
	}
	else {
		i_c = &empty_val[0];
	}
	return i_c;
}

void convert_bin(int input_n, int bin_len, int *bin_n) {

	int n = abs(input_n);

	int shift = bin_len - 1;
	
	for (int i = 0; i < bin_len; i++) {
		unsigned int val = n << i;
		val = val >> shift;
		bin_n[i] = val;
	}

	if (input_n < 0) {

		for (int j = 0; j < bin_len; j++) {
			bin_n[j] = (bin_n[j] == 1) ? 0 : 1;
		}
		int k = bin_len-1;
		while (k >= 0 && bin_n[k] != 0) {
			bin_n[k] = 0;
			k--;
		}
		bin_n[k] = 1;
	}
}

int bit_xor(int n, int i) {
	return n ^ i;
}

int bit_or(int n, int i) {
	return n | i;
}

int bit_and(int n, int i) {
	return n & i;
}

int bit_not(int n, int i) {
	return ~i;
}

int bit_left(int n, int i) {
	if (i < 0) {
		printf("ERR: attempt to bit-shift negative value.\n");
		exit(1);
	}
	return i << n;
}

int bit_right(int n, int i) {
	if (i < 0) {
		printf("ERR: attempt to bit-shift negative value.\n");
		exit(1);
	}
	return i >> n;
}

char **alloc_2D_char_null(int l) {

	char **arr_ptr = (char**)malloc(sizeof(char*) * l);
	for (int i = 0; i < l; i++) {
		arr_ptr[i] = NULL;
	}
	return arr_ptr;
}

int *alloc_int_arr(int l) {

	int *arr_ptr = (int*)malloc(sizeof(int) * l);
	for (int i = 0; i < l; i++) {
		arr_ptr[i] = 0;
	}
	return arr_ptr;
}

int **alloc_2D_int(int l, int bin_len) {

	int **arr_ptr = (int**)malloc(sizeof(int*) * l);
	for (int i = 0; i < l; i++) {
		arr_ptr[i] = (int*)malloc(sizeof(int) * bin_len);
	}
	return arr_ptr;
}

int check_opt_params(int argc, char **argv, 
		Chart_params_t *input_params, int index) {
	
	int limit_bool = 0;
	int compare_bool = 0;
	int range_bool = 0;

	char compare[12] = "compare";
	char limit[12] = "limit";
	char range[12] = "range";

	while (index < argc) {

		if (compare_bool && limit_bool) {
			return -4;
		}
		else if (compare_bool == 0 && limit_bool == 0 &&
					!strcmp(compare, argv[index])) {
			compare_bool = 1;
			input_params -> compare_bool = 1;
			index++;
		}
		else if (limit_bool == 0 && compare_bool == 0 &&
					!strcmp(limit, argv[index])) {
			limit_bool = 1;
			input_params -> limit_bool = 1;
			index++;
			
			if (index >= argc) {
				return -6;
			}

			int n = check_number(argv[index]);

			if (n < -999 || n > 999) {
				return -6;
			}

			input_params -> limit_val = n;
			index++;
		}
		else if (range_bool == 0 && !strcmp(range, argv[index])) {
			
			int start;
			int end;
			range_bool = 1;
			index++;
			
			if (index >= argc) {
				return -9;
			}

			start = check_number(argv[index]);
			
			if (start < -127 || start > 255) {
				return -7;
			}
			
			input_params -> range_start = start;
			index++;

			if (index >= argc) {
				input_params -> range_end = start;
				return 0;
			}

			end = check_number(argv[index]);
			
			if (end < start || end > 255) {
				return -8;
			}
				
			input_params -> range_end = end;
			index++;
		}
		else {
			return -5;
		}
	}
	return 0;
}


int check_input_val(char *input_val, int op_idx) {

	int n = check_number(input_val);
			
	if (op_idx == 0 || op_idx == 1 || op_idx == 2) {
		if (n > -999 && n < 999) {
			return n;
		}
		else {
			return -2;
		}
	}
	else if (op_idx == 4 || op_idx == 5) {
		if (n > -1 && n < (sizeof(int) * 8)) {
			return n;
		}
		else {
			return -3;
		}
	}
	else {
		return -99;
	}
}

int process_params(int argc, char **argv, 
		   int op_idx, Chart_params_t *input_params) {
	
	if (argc < 3) {
		if (op_idx == 3) return 0;
		else if (op_idx == 0 || op_idx == 1 || op_idx == 2) {
			return -1;
		}
		else if (op_idx == 4 || op_idx == 5) {
			return -10;
		}
		else return -99;
	}

	int index = 2;
	if (op_idx != 3) {

		int check_val = check_input_val(argv[2], op_idx);

		if (check_val < 0) {
			return check_val;
		}

		input_params -> input_val = check_val;
		index++;
	}

	if (index == argc) return 0;
	else {
		int check_val = check_opt_params(argc, argv, 
					input_params, index);
		return check_val;
	}
	return 0;
}

int is_valid_oper(char *oper, Chart_params_t *input_params) {

	char or[6] = {'o', 'r', '\0'};
	char or_2[6] = {'|', '\0'};
	char and[6] = {'a', 'n', 'd', '\0'};
	char and_2[6] = {'&', '\0'};
	char xor[6] = {'x', 'o', 'r', '\0'};
	char xor_2[6] = {'^', '\0'};
	char not[6] = {'n', 'o', 't', '\0'};
	char not_2[6] = {'~', '\0'};
	char left[6] = {'l', 'e', 'f', 't', '\0'};
	char left_2[6] = {'<', '<', '\0'};
	char right[6] = {'r', 'i', 'g', 'h', 't', '\0'};
	char right_2[6] = {'>', '>', '\0'};

	if (!strcmp(oper, or) || !strcmp(oper, or_2)) {
	 	return 0;
	}
	else if (!strcmp(oper, and) || !strcmp(oper, and_2)) {
		return 1;
	}
	else if (!strcmp(oper, xor) || !strcmp(oper, xor_2)) {
		return 2;
	}
	else if (!strcmp(oper, not) || !strcmp(oper, not_2)) {
		return 3;
	}
	else if (!strcmp(oper, left) || !strcmp(oper, left_2)) {
		return 4;
	}
	else if (!strcmp(oper, right) || !strcmp(oper, right_2)) {
		return 5;
	}
	else {
		return -1;
	}
}

void print_manual() {
	printf("\n\tThis is an ASCII bitwise chart printer."
	"\n\n\tRequired params (\033[38;5;45m1\033[0m):"
	"\n\t\t \033[38;5;45m\033[3m[char]\033[0m\033[0m"
	"operation_name\n\n\tOptional params(\033[38;5;45m6\033[0m):"
	"\n\t\t\033[38;5;45m\033[3m[int]\033[0m\033[0m"
	"ASCII/bit-shift \033[38;5;45m\033[3m[char]\033[0m\033[0m"
	"highlight \033[38;5;45m\033[3m[int]\033[0m\033[0m"
	"limit_value \n\t\trange "
	"\033[38;5;45m\033[3m[int]\033[0m\033[0mrange_st "
	"\033[38;5;45m\033[3m[int]\033[0m\033[0mrange_end"
	"\n\n\toperation_name options:"
	"\n\t\txor, or, and, not, left, right"
	"\n\t\t\"^\", \"|\", \"&\", \"~\", \"<<\", \">>\""
	"\n\n\tRange values:\n\t\t"
	"-127 ... 255\n\n\tBit-shift values:\n\t\t0 ... 31\n\n\t"
	"highlight options:\n\t\tcompare/limit"
	"\n\n\tInput value and limit_value:\n\t\t-999 ... 999\n\n\t"
	"\033[3mASCII_code\033[0m is only optional for the "
	"\033[3mnot\033[0m operation.\n\t\033[3mcompare\033[0m "
	"highlights results based on previous value.\n\t"
	"\033[3mlimit\033[0m highlights "
	"result based on \033[3mlimit_value\033[0m parameter."
	"\n\tDefault range is 0 ... 127."
	"\n\tOnly 8 or 16 least significant bits are shown."
	"\n\n\tExamples: ./bit_chart xor 127 limit 64 range 32 127"
	"\n\n\t\t  ./bit_chart \"~\" compare\n\n");
}

void print_err_n(int val) {

	print_manual();
	printf("\033[38;5;196m ERROR: \033[0m");

	switch (val) {

		case 0:
			printf("Invalid number of parameters.\n\n");
		break;

		case -1:
			printf("No ASCII value.\n\n");
		break;

		case -2:
			printf("Value must be between -999 and 999.\n\n");
		break;

		case -3:
			printf("Bit-shift must be between 0 and 31.\n\n");
		break;

		case -4:
			printf("limit/compare are exclusive.\n\n");
		break;

		case -5:
			printf("Invalid optional parameter.\n\n");
		break;

		case -6:
			printf("Limit must be between -999 and 999.\n\n");
		break;

		case -7:
			printf("Invalid range start (-127 ... 255).\n\n");
		break;

		case -8:
			printf("Invalid range end (-127 ... 255).\n\n");
		break;

		case -9:
			printf("Range parameter missing.\n\n");
		break;

		case -10:
			printf("No bit-shift value.\n\n");
		break;

		case -11:
			printf("Invalid operation.\n\n");
		break;

		case -99:
			printf("Not supposed to happen lol.\n\n");
		break;

		default:
			printf("Not supposed to happen lol.\n\n");
		break;
	}
}


int main(int argc, char **argv) {

	if (argc < 2 || argc > 8) {
		print_err_n(0);
		return 0;
	}

		/* Assign bitwise operation */

	typedef int bitwise_op_t(int, int);
	
	bitwise_op_t *bitwise_arr[6] = {bit_or, bit_and, bit_xor, 
					bit_not, bit_left, bit_right};
		
	char operation[6];
	char *op_ptr;
	op_ptr = strncpy(operation, argv[1], strlen(argv[1])+1);

	bitwise_op_t *input_op;
	
	Chart_params_t input_params;

	{
	input_params.input_val = 0;
	
	input_params.bin_start = (sizeof(int) * 8) - (sizeof(int) * 2);

	input_params.limit_bool = 0;
	input_params.limit_val = 0;
	input_params.compare_bool = 0;
	
	input_params.range_start = 0;
	input_params.range_end = 127;
	}
	
	int op_idx = is_valid_oper(&operation[0], &input_params);

	if (op_idx < 0) {
		print_err_n(-11);
		return 0;
	}

	input_op = bitwise_arr[op_idx];


		/* Other input parameter assignments */	
	
	int params_val = process_params(argc, argv, op_idx, 
					&input_params);

	if (params_val < 0) {
		print_err_n(params_val);
		return 0;
	}
	else {
		for (int i = 1; i < argc; i++) {
			printf("%s ", argv[i]);
		}
	}

		/* Font colors */

	char white[16] = "\033[38;5;123m\0";

	char color[16];
	char *text_color;

	text_color = strncpy(color, white, 16);

		/* Background colors */

	char bg_blue[16] = "\033[48;5;17m\0";
	char bg_red[16] = "\033[48;5;52m\0";
	char bg_orange[16] = "\033[48;5;136m\0";
	char reset[16] = "\033[0m\0";
	
	char bg_color[16];
	char *highlight_text;

	highlight_text = strncpy(bg_color, reset, 16);

	printf("\n\n");


	int bin_len = sizeof(int) * 8;

		/* Range variables */

	int range_start = input_params.range_start;
	int range_end = input_params.range_end;
	int range_len = abs(range_start + (-input_params.range_end)-1);

		/* Allocate arrays */

	char **bg_color_arr = alloc_2D_char_null(range_len);
	char **color_arr = alloc_2D_char_null(range_len);

	int *decimal_a = alloc_int_arr(range_len);
	int range_n = range_start;

	int n = input_params.input_val;
		
		/* Calculate decimal chart */

	for (int i = 0; i < range_len; i++) {

		int a = (*input_op)(n, range_n);
		decimal_a[i] = a;

		if (a > 255 || a < -255) {
			input_params.bin_start = bin_len / 2;
		}
		range_n++;
	}
	
		/* Get terminal size */

	int bin_start = input_params.bin_start;
	int field_w = ((bin_len - bin_start) * 2) + 4;

	struct winsize term_size;
	ioctl(STDOUT_FILENO, TIOCGWINSZ, &term_size);
	
	int line_cols = term_size.ws_col / field_w;		

		/* Calculate binary chart */

	int **binary_i = alloc_2D_int(range_len, bin_len);
	int **binary_a = alloc_2D_int(range_len, bin_len);

	range_n = range_start;
	
	for (int i = 0; i < range_len; i++) {

		convert_bin(range_n, bin_len, &binary_i[i][0]);
		convert_bin(decimal_a[i], bin_len, &binary_a[i][0]);
		
		color_arr[i] = &white[0];
		bg_color_arr[i] = &reset[0];
		range_n++;
	}

		/* Highlight text */

	if (input_params.compare_bool == 1) {
		
		int prev = 0;
		if (op_idx < 4 || range_start > 0) {
			prev = (*input_op)(n, range_start - 1);
		}
		
		for (int i = 0; i < range_len; i++) {
			int a = decimal_a[i];
			if (a > prev) {
				bg_color_arr[i] = &bg_red[0];
			}
			else if (a == prev) {
				bg_color_arr[i] = &bg_orange[0];
			}
			else {
				bg_color_arr[i] = &bg_blue[0];
			}
			prev = a;
		}
	}
	else if (input_params.limit_bool == 1) {
		int limit_value = input_params.limit_val;
		for (int i = 0; i < range_len; i++) {
			int a = decimal_a[i];
			if (a > limit_value) {
				bg_color_arr[i] = &bg_red[0];
			}
			else if (a == limit_value) {
				bg_color_arr[i] = &bg_orange[0];
			}
			else {
				bg_color_arr[i] = &bg_blue[0];
			}
		}
	}

		/* Render chart */

	int col_count = 0;

	range_n = range_start;
	int last_index = range_len - 1;
	
	int field_len = field_w - 1;
	char *field = (char*)malloc(sizeof(char) * (field_len));

	for (int i = 0; i < field_len; i++) {
		field[i] = ' ';
	}
	field[field_len] = '\0';

	for (int i = 0; i < range_len; i++) {
	
		int a = decimal_a[i];

		highlight_text = strncpy(bg_color, bg_color_arr[i], 16);
		text_color = strncpy(color, color_arr[i], 16);

		char *i_c = get_ASCII_value(range_n);
		char *a_c = get_ASCII_value(a);

		fill_field(range_n, i_c, a, a_c, field_w, field);


		printf("%s%s%s", bg_color, color, field);
		
		highlight_text = strncpy(bg_color, reset, 16);
		printf("%s ", bg_color);
		
		col_count++;

		for (int f_idx = 0; f_idx < field_len; f_idx++) {
			field[f_idx] = ' ';
		}

			/* Print binaries */

		if (col_count % line_cols == 0 || i == last_index) {

			printf("\n");
			int j = i - line_cols + 1;
			int i_len = i + 1;
			if (col_count % line_cols != 0) {
				j = last_index - col_count + 1;
				i_len = last_index+1;
			}
			for (j; j < i_len; j++) {
				
				printf("%s%s ", bg_color_arr[j], 
						color_arr[j]);
				
				print_binaries(bin_start, bin_len, 
						&binary_i[j][0]);
				print_binaries(bin_start, bin_len, 
						&binary_a[j][0]);

				highlight_text = strncpy(bg_color, 
						reset, 16);
				printf("%s ", bg_color);
			}
		printf("\n\n");
		col_count = 0;
		}
		range_n++;
	}
	highlight_text = strncpy(bg_color, reset, 16);
	printf("%s\n", bg_color);
	
		/* Free allocated memory */

	for (int i = 0; i < range_len; i++) {
		free(binary_i[i]);
		free(binary_a[i]);
	}
	free(field);
	free(decimal_a);
	free(bg_color_arr);
	free(color_arr);
	free(binary_i);
	free(binary_a);

	return 0;
}
